#include <iostream>

#define null 0
#define besar_pajak 0.25
#define satuan_tunj 100000
#define gaji_kotor 250000

using namespace std;

int main() {
	float gaji_bersih, pajak, tunjangan_anak = 0;
	int status, jumlah_anak = 0,gol,xgol;
	string nama,jabatan,choose = "N"; 
	
	input:	
		cout << "Masukan Nama : "; getline(cin,nama);
		gol:
			cout << "1. Golongan Pertama\n";
			cout << "2. Golongan Kedua\n";
			cout << "3. Golongan Ketiga\n";
		
		cout << "Pilih Golongan : "; cin >> gol;
		
		cout << endl;
		
		status:
			cout << "1. Menikah\n";
			cout << "2. Lajang\n";
			
		cout << "Pilih Status : ";	
		cin >> status;      
		
		if(status < 2) {
			cout << "Jumlah Anak : "; cin >> jumlah_anak;	
		}
	
	if(gol > null && gol < 4) {
		if(jumlah_anak > 0) {
			tunjangan_anak = jumlah_anak * satuan_tunj;
		} 
			
		if(gol == 1) {
			xgol = 1;
		} else if(gol == 2) {
			xgol = 2;
		} else {
			xgol = 3;
		}

		pajak = (gaji_kotor * xgol) * besar_pajak;
		gaji_bersih = ((gaji_kotor * xgol) + tunjangan_anak) -pajak;	

		system("cls");
	} else {
		cout << "Maaf Pilihan Anda Tidak Tersedia! Ulang Kembali?";

		if(choose != "N") {
			system("cls");
			goto input;
		}
	}
	
	
	cout << " | Rincian Gaji Sdr " << nama << " |" << endl << endl;
	cout << " | Gaji Bersih\t\t" << " : Rp." << gaji_bersih << "\t |" << endl;
	cout << " | Tunjangan Anak\t" << " : Rp." << tunjangan_anak << "\t |" << endl;
	cout << " | Pajak\t\t" << " : Rp." << pajak << "\t |" << endl << endl;
	cout << " \nApakah Anda Ingin Kembali? " << " [Y/N]" << endl; cin >> choose;
	
	if(choose != "N") {
		system("cls");
		goto input;
	}
	
	return 0;
}
